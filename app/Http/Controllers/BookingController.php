<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookingController extends Controller
{
    /**
     * Get all bookings.
     *
     * @return Collection
     */
    public function index()
    {
        return Booking::all();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $payload = $this->getValidatedData($request);

        $client = Client::find($request->get('client_id'));
        $dogs = $client->dogs()->get();

        $sumAge = 0;
        foreach ($dogs as $dog){
            $sumAge += $dog->age;
        }

        if(($sumAge / count($dogs)) < 10){
            $payload['price'] = $payload['price'] * 0.9;
        }
        return response()->json(Booking::create($payload), 201);
    }

    /**
     * Get validated data.
     *
     * @param Request $request
     *
     * @return array
     */
    private function getValidatedData(Request $request): array
    {
        return $request->validate([
            'client_id' => [
                'required'
            ],
            'price' => [
                'required',
                'integer',
            ],
            'check_in_date' => [
                'required',
                'date'
            ],
            'check_out_date' => [
                'required',
                'date'
            ],
        ]);
    }
}
